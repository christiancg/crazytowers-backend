package com.crazytowers.handlers.request;

import com.crazytowers.handlers.Actions;
import lombok.Data;

@Data
public class BaseAction {
	private Actions type;
}

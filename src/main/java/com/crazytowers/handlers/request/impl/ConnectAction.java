package com.crazytowers.handlers.request.impl;

import com.crazytowers.handlers.request.BaseAction;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class ConnectAction extends BaseAction {
	private String name;
	private boolean isMobile;
}

package com.crazytowers.handlers.request.impl;

import com.crazytowers.handlers.request.BaseAction;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
public class MoveAction extends BaseAction {
    private double heading;
    private double posX;
    private double posZ;
}

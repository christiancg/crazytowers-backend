package com.crazytowers.handlers.response.impl;

import com.crazytowers.handlers.response.BaseResponse;
import com.crazytowers.handlers.Actions;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class DeadResponse extends BaseResponse {
    public DeadResponse() {
        super(Actions.DEAD, false);
    }
}

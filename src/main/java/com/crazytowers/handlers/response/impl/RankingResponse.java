package com.crazytowers.handlers.response.impl;

import com.crazytowers.data.PlayerRankingEntry;
import com.crazytowers.handlers.Actions;
import com.crazytowers.handlers.response.BaseResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class RankingResponse extends BaseResponse {
    private List<PlayerRankingEntry> ranking;

    private RankingResponse() {
        super(Actions.RANKING, true);
    }

    public RankingResponse(List<PlayerRankingEntry> ranking){
        super(Actions.RANKING, true);
        new RankingResponse();
        this.ranking = ranking;
    }
}

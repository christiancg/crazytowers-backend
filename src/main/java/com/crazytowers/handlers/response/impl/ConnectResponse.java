package com.crazytowers.handlers.response.impl;

import com.crazytowers.handlers.Actions;
import com.crazytowers.handlers.response.BaseResponse;
import com.crazytowers.model.Player;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class ConnectResponse extends BaseResponse {
    private int mapSideSize;
    private int positionRefreshIntervalInMs;
    private int gathererPenalizationInMs;
    private int graceTimeInMs;
    private Player player;

    private ConnectResponse() {
        super(Actions.CONNECT, true);
    }

    public ConnectResponse(int mapSideSize, int positionRefreshIntervalInMs, int gathererPenalizationInMs, int graceTimeInMs, Player player) {
        super(Actions.CONNECT, true);
        new ConnectResponse();
        this.mapSideSize = mapSideSize;
        this.positionRefreshIntervalInMs = positionRefreshIntervalInMs;
        this.gathererPenalizationInMs = gathererPenalizationInMs;
        this.graceTimeInMs = graceTimeInMs;
        this.player = player;
    }
}

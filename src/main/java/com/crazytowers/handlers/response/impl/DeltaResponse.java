package com.crazytowers.handlers.response.impl;

import com.crazytowers.handlers.Actions;
import com.crazytowers.handlers.response.BaseResponse;
import com.crazytowers.model.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class DeltaResponse extends BaseResponse {
    private Player player;
    private List<Gatherer> nearbyGatherers;
    private List<Tower> nearbyTowers;
    private List<Brick> nearbyBricks;
    private List<AttackBrick> nearbyAttackBricks;

    private DeltaResponse() {
        super(Actions.DELTA, true);
    }

    public DeltaResponse(Player player, List<Gatherer> nearbyGatherers, List<Tower> nearbyTowers, List<Brick> nearbyBricks, List<AttackBrick> nearbyAttackBricks) {
        super(Actions.DELTA, true);
        new DeltaResponse();
        this.player = player;
        this.nearbyGatherers = nearbyGatherers;
        this.nearbyTowers = nearbyTowers;
        this.nearbyBricks = nearbyBricks;
        this.nearbyAttackBricks = nearbyAttackBricks;
    }
}

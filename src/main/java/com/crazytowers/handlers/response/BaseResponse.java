package com.crazytowers.handlers.response;

import com.crazytowers.handlers.Actions;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class BaseResponse {
    private Actions type;
    private boolean status;
}

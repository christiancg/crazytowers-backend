package com.crazytowers.handlers;

import com.crazytowers.Utils.GsonExclusionStrategy;
import com.crazytowers.Utils.PositionUtils;
import com.crazytowers.Utils.auxiliary.Position;
import com.crazytowers.config.ApplicationConfig;
import com.crazytowers.handlers.request.BaseAction;
import com.crazytowers.handlers.request.impl.ConnectAction;
import com.crazytowers.handlers.request.impl.MoveAction;
import com.crazytowers.handlers.response.BaseResponse;
import com.crazytowers.handlers.response.impl.DeadResponse;
import com.crazytowers.model.Player;
import com.crazytowers.service.IProcessActionService;
import com.crazytowers.service.IRealmService;
import com.crazytowers.service.ISaveService;
import com.crazytowers.service.IShowService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import jakarta.annotation.Nonnull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.socket.WebSocketHandler;
import org.springframework.web.reactive.socket.WebSocketMessage;
import org.springframework.web.reactive.socket.WebSocketSession;
import reactor.core.Disposable;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

@Component
public class ReactiveWebSocketHandler implements WebSocketHandler {
    private final static Map<String, Disposable> intervalDeltaMap = new ConcurrentHashMap<>();
    private final static Map<String, Disposable> intervalRankingMap = new ConcurrentHashMap<>();
    private final IShowService showService;
    private final ISaveService saveService;
    private final IProcessActionService pas;
    private final PositionUtils positionUtils;
    private final IRealmService realmService;
    private final ApplicationConfig applicationConfig;
    private final Gson gson = new GsonBuilder().addSerializationExclusionStrategy(new GsonExclusionStrategy()).create();

    @Autowired
    public ReactiveWebSocketHandler(IShowService showService, ISaveService saveService, IProcessActionService pas, PositionUtils positionUtils, IRealmService realmService, ApplicationConfig applicationConfig) {
        this.showService = showService;
        this.saveService = saveService;
        this.pas = pas;
        this.positionUtils = positionUtils;
        this.realmService = realmService;
        this.applicationConfig = applicationConfig;
    }

    @Override
    @Nonnull
    public Mono<Void> handle(WebSocketSession session) {
        return session.receive().map(WebSocketMessage::getPayloadAsText).handle((msg, sink) -> {
            BaseAction action = gson.fromJson(msg, BaseAction.class);
            switch (action.getType()) {
                case CONNECT -> connectMessage(msg, session);
                case MOVE -> processMoveAndSendDelta(msg, session);
                case CLOSE_ATTACK -> respondOrDisconnect(session, pas::closeAttack);
                case RANGE_ATTACK -> respondOrDisconnect(session, pas::rangeAttack);
                case DEFEND -> respondOrDisconnect(session, pas::defend);
                case TOGGLE_MOVE_TOWER -> respondOrDisconnect(session, pas::toggleMoveTower);
                case STOP_MOVE -> respondOrDisconnect(session, pas::stopMove);
                default -> actionNotSupported(session);
            }
        }).doOnTerminate(() -> terminateConnectionAndReleaseResources(session.getId())).then();
    }

    private void terminateConnectionAndReleaseResources(String sessionId) {
        saveService.removeWhenDisconnected(sessionId);
        if (intervalDeltaMap.containsKey(sessionId)) {
            var disposable = intervalDeltaMap.get(sessionId);
            disposable.dispose();
            intervalDeltaMap.remove(sessionId);
        }
        if (intervalRankingMap.containsKey(sessionId)) {
            var disposable = intervalRankingMap.get(sessionId);
            disposable.dispose();
            intervalRankingMap.remove(sessionId);
        }
        pas.removeMoveIntervalFlux(sessionId);
    }

    private boolean validateName(String name) {
        return name != null && name.length() >= 3 && name.length() <= 20;
    }

    private void connectMessage(String msg, WebSocketSession session) {
        ConnectAction connMsg = gson.fromJson(msg, ConnectAction.class);
        if (validateName(connMsg.getName())) {
            Player player = new Player(session.getId(), connMsg.getName(), connMsg.isMobile());
            player.setId(UUID.randomUUID());
            player.setRealm(realmService.getAvailableRealm());
            Position randomPos = positionUtils.getRandomPositionInMap();
            var tower = saveService.saveNewTower(player, randomPos.getPosX(), randomPos.getPosZ());
            tower.setUpGraceTime(applicationConfig.getGraceTimeInMs());
            player.setTower(tower);
            var gatherer = saveService.saveNewGatherer(player, randomPos.getPosX(), randomPos.getPosZ());
            gatherer.setUpGraceTime(applicationConfig.getGraceTimeInMs());
            player.setGatherer(gatherer);
            saveService.saveNewPlayer(player);
            registerIntervalFluxes(session, player);
            mapSendAndSubscribe(pas.getConnectResponse(player), session, false);
        } else {
            var deadResponse = new DeadResponse();
            mapSendAndSubscribe(Mono.just(deadResponse), session, true);
        }
    }

    private void registerIntervalFluxes(WebSocketSession session, Player player) {
        var deltaFlux = pas.getDeltaIntervalFlux(session.getId())
                .doOnError(throwable -> mapSendAndSubscribe(Mono.just(new DeadResponse()), session, true)).parallel()
                .runOn(Schedulers.boundedElastic());
        var disposableDelta = session.send(deltaFlux.map(gson::toJson).map(session::textMessage)).subscribe();
        intervalDeltaMap.put(session.getId(), disposableDelta);
        var rankingFlux = pas.getRankingInterval(player).doOnError(throwable -> {
            terminateConnectionAndReleaseResources(session.getId());
            var deadResponse = Mono.just(new DeadResponse());
            mapSendAndSubscribe(deadResponse, session, true);
        }).parallel().runOn(Schedulers.boundedElastic());
        var disposableRanking = session.send(rankingFlux.map(gson::toJson).map(session::textMessage)).subscribe();
        intervalRankingMap.put(session.getId(), disposableRanking);
    }

    private void processMoveAndSendDelta(String msg, WebSocketSession session) {
        MoveAction moveMsg = gson.fromJson(msg, MoveAction.class);
        var player = getPlayerOrSendDeadMessageIfNotPresent(session);
        if (player != null) {
            mapSendAndSubscribe(pas.move(player, moveMsg), session, false);
        }
    }

    private void respondOrDisconnect(WebSocketSession session, Function<Player, Mono<BaseResponse>> functionToApply) {
        var player = getPlayerOrSendDeadMessageIfNotPresent(session);
        if (player != null)
            mapSendAndSubscribe(functionToApply.apply(player), session, false);
    }

    private void actionNotSupported(WebSocketSession session) {
        mapSendAndSubscribe(Mono.just(new BaseResponse(Actions.NOT_SUPPORTED, false)), session, false);
    }

    private Player getPlayerOrSendDeadMessageIfNotPresent(WebSocketSession session) {
        var player = showService.getPlayer(session.getId());
        if (player == null) {
            terminateConnectionAndReleaseResources(session.getId());
            var deadResponse = new DeadResponse();
            mapSendAndSubscribe(Mono.just(deadResponse), session, true);
        }
        return player;
    }

    private void mapSendAndSubscribe(Mono<?> toSend, WebSocketSession session, boolean disconnect) {
        var messageTransporter = session.send(toSend.map(gson::toJson).map(session::textMessage));
        if (disconnect)
            messageTransporter.and(session.close()).subscribe();
        else
            messageTransporter.subscribe();
    }
}

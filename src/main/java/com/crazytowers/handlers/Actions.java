package com.crazytowers.handlers;

import com.google.gson.annotations.SerializedName;

public enum Actions {
    @SerializedName("connect")
    CONNECT("connect"),
    @SerializedName("delta")
    DELTA("delta"),
    @SerializedName("ranking")
    RANKING("ranking"),
    @SerializedName("dead")
    DEAD("dead"),
    @SerializedName("move")
    MOVE("move"),
    @SerializedName("closeAttack")
    CLOSE_ATTACK("closeAttack"),
    @SerializedName("rangeAttack")
    RANGE_ATTACK("rangeAttack"),
    @SerializedName("defend")
    DEFEND("defend"),
    @SerializedName("toggleMoveTower")
    TOGGLE_MOVE_TOWER("toggleMoveTower"),
    @SerializedName("stopMove")
    STOP_MOVE("stopMove"),
    @SerializedName("notSupported")
    NOT_SUPPORTED("notSupported");

    private final String type;

    Actions(final String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return type;
    }
}

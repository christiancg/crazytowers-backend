package com.crazytowers.data;

import lombok.Data;

import java.util.UUID;

@Data
public class PlayerRankingEntry {
    private UUID id;
    private String name;
    private int bricks;
    private int position;

    public PlayerRankingEntry(UUID id, String name, int bricks) {
        this.id = id;
        this.name = name;
        this.bricks = bricks;
    }
}

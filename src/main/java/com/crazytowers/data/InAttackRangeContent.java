package com.crazytowers.data;

import com.crazytowers.model.Gatherer;
import com.crazytowers.model.Tower;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class InAttackRangeContent {
    List<Gatherer> gatherers;
    List<Tower> towers;
}

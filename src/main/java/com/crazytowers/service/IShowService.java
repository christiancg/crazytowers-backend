package com.crazytowers.service;

import com.crazytowers.data.InAttackRangeContent;
import com.crazytowers.handlers.response.impl.DeltaResponse;
import com.crazytowers.handlers.response.impl.RankingResponse;
import com.crazytowers.model.AttackBrick;
import com.crazytowers.model.Gatherer;
import com.crazytowers.model.Player;
import com.crazytowers.model.Tower;
import reactor.core.publisher.Mono;

import java.util.UUID;

public interface IShowService {
    Player getPlayer(String sessionId);

    Player searchPlayerByUUID(UUID id);

    Gatherer getGatherer(int realm, UUID playerId);

    Tower getTower(int realm, UUID playerId);

    Mono<DeltaResponse> getShowableContent(Player player);

    Mono<InAttackRangeContent> getInAttackRangeContent(Player player, AttackBrick brick);

    Mono<RankingResponse> getRankingInterval(Player player);
}

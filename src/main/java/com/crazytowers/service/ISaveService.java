package com.crazytowers.service;

import com.crazytowers.model.*;

public interface ISaveService {
    Gatherer saveNewGatherer(Player player, double posX, double posZ);

    Tower saveNewTower(Player player, double posX, double posZ);

    Brick saveNewBrick(int realm, boolean large, double posX, double posZ, boolean dropped);

    AttackBrick saveNewAttackBrick(int realm, double posX, double posZ, double heading, double speed, double distanceToCover);

    void saveNewPlayer(Player player);

    void removeWhenDisconnected(String sessionId);

    void removeBrick(int realm, Brick toRemove);

    void removeAttackBrick(int realm, AttackBrick toSave);
}

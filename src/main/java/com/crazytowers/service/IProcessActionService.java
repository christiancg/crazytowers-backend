package com.crazytowers.service;

import com.crazytowers.handlers.request.impl.MoveAction;
import com.crazytowers.handlers.response.BaseResponse;
import com.crazytowers.handlers.response.impl.ConnectResponse;
import com.crazytowers.handlers.response.impl.DeltaResponse;
import com.crazytowers.handlers.response.impl.RankingResponse;
import com.crazytowers.model.Player;
import reactor.core.Disposable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface IProcessActionService {
    Flux<DeltaResponse> getDeltaIntervalFlux(final String sessionId);

    Flux<RankingResponse> getRankingInterval(Player player);

    void removeMoveIntervalFlux(String sessionId);

    Disposable registerMoveIntervalFlux(Player sessionId);

    Disposable registerRangeAttackIntervalFlux(Player player);

    Mono<BaseResponse> move(Player player, MoveAction moveAction);

    Mono<BaseResponse> stopMove(Player player);

    Mono<BaseResponse> closeAttack(Player player);

    Mono<BaseResponse> rangeAttack(Player player);

    Mono<BaseResponse> defend(Player player);

    Mono<BaseResponse> toggleMoveTower(Player player);

    Mono<ConnectResponse> getConnectResponse(Player player);

    Mono<DeltaResponse> getDelta(String sessionId);
}

package com.crazytowers.service;

import com.crazytowers.data.InAttackRangeContent;
import com.crazytowers.handlers.request.impl.MoveAction;
import com.crazytowers.handlers.response.impl.DeltaResponse;
import com.crazytowers.model.AttackBrick;
import com.crazytowers.model.Player;

public interface ICollisionService {
    DeltaResponse detectCollisionsAndReact(Player movedPlayer, DeltaResponse showableContent, MoveAction action);

    boolean closeAttack(Player player, DeltaResponse showableContent);

    boolean brickAttack(int realm, AttackBrick brick, InAttackRangeContent attackableContent);

    boolean defend(Player player, DeltaResponse showableContent);
}

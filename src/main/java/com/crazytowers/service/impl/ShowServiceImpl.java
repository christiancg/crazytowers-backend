package com.crazytowers.service.impl;

import com.crazytowers.Utils.GeometryUtils;
import com.crazytowers.config.ApplicationConfig;
import com.crazytowers.dao.IEntityCacheDAO;
import com.crazytowers.data.InAttackRangeContent;
import com.crazytowers.handlers.response.impl.DeltaResponse;
import com.crazytowers.handlers.response.impl.RankingResponse;
import com.crazytowers.model.*;
import com.crazytowers.service.IShowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

@Service
public class ShowServiceImpl implements IShowService {

    private final ApplicationConfig applicationConfig;
    private final IEntityCacheDAO entityCacheDAO;
    private final GeometryUtils geometryUtils;

    @Autowired
    public ShowServiceImpl(ApplicationConfig applicationConfig, IEntityCacheDAO entityCacheDAO, GeometryUtils geometryUtils) {
        this.applicationConfig = applicationConfig;
        this.entityCacheDAO = entityCacheDAO;
        this.geometryUtils = geometryUtils;
    }

    private Flux<Tower> getShowableTowers(Player player) {
        final var auxTower = entityCacheDAO.getTower(player.getRealm(), player.getId());
        if (auxTower != null)
            return entityCacheDAO.getNearbyTowers(player.getRealm(), auxTower.getPosX(), auxTower.getPosZ(), player.getMaximumVisibleRange(), auxTower.getId());
        else
            return Flux.empty();
    }

    private Flux<Gatherer> getShowableGatherers(Player player) {
        final var auxTower = entityCacheDAO.getTower(player.getRealm(), player.getId());
        if (auxTower != null)
            return entityCacheDAO.getNearbyGatherers(player.getRealm(), auxTower.getPosX(), auxTower.getPosZ(), player.getMaximumVisibleRange(), auxTower.getId());
        else
            return Flux.empty();
    }

    private Flux<Brick> getShowableBricks(Player player) {
        final var auxTower = entityCacheDAO.getTower(player.getRealm(), player.getId());
        if (auxTower != null)
            return entityCacheDAO.getNearbyBricks(player.getRealm(), auxTower.getPosX(), auxTower.getPosZ(), player.getMaximumVisibleRange());
        else
            return Flux.empty();
    }

    private Flux<AttackBrick> getShowableAttackBricks(Player player) {
        final var auxTower = entityCacheDAO.getTower(player.getRealm(), player.getId());
        if (auxTower != null)
            return entityCacheDAO.getNearbyAttackBricks(player.getRealm(), auxTower.getPosX(), auxTower.getPosZ(), player.getMaximumVisibleRange());
        else
            return Flux.empty();
    }

    @Override
    public Mono<DeltaResponse> getShowableContent(Player player) {
        if (player != null) {
            player.setMaximumVisibleRange(geometryUtils
                    .getMaximumRangeForHeight(player.getTower().getBricks()));
            return Mono.zip(getShowableTowers(player).collectList(), getShowableGatherers(player).collectList(), getShowableBricks(player)
                    .collectList(), getShowableAttackBricks(player).collectList())
                    .map(x -> new DeltaResponse(player, x.getT2(), x.getT1(), x.getT3(), x.getT4()));
        } else
            return Mono.empty();
    }

    @Override
    public Mono<InAttackRangeContent> getInAttackRangeContent(Player player, AttackBrick brick) {
        return Mono.zip(entityCacheDAO.getNearbyGatherers(player.getRealm(), brick.getPosX(), brick.getPosZ(), 10, player.getId())
                .collectList(), entityCacheDAO.getNearbyTowers(player.getRealm(), brick.getPosX(), brick.getPosZ(), 10, player.getId())
                .collectList()).map(objects -> new InAttackRangeContent(objects.getT1(), objects.getT2()));
    }

    @Override
    public Mono<RankingResponse> getRankingInterval(Player player) {
        if (player != null && player.getLastActivity() > System.currentTimeMillis() - applicationConfig.getSessionTimeoutInMs())
            return entityCacheDAO.getRankingList(player.getRealm(), player.getId()).collectList().map(RankingResponse::new);
        else
            return Mono.error(new Exception("Disconnected by inactivity" + (player != null ? ": " + player.getName() : "")));
    }

    @Override
    public Player getPlayer(String sessionId) {
        return entityCacheDAO.getPlayer(sessionId);
    }

    @Override
    public Player searchPlayerByUUID(UUID id) {
        return entityCacheDAO.getPlayer(id);
    }

    @Override
    public Gatherer getGatherer(int realm, UUID playerId) {
        return entityCacheDAO.getGatherer(realm, playerId);
    }

    @Override
    public Tower getTower(int realm, UUID playerId) {
        return entityCacheDAO.getTower(realm, playerId);
    }

}

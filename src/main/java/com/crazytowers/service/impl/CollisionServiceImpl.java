package com.crazytowers.service.impl;

import com.crazytowers.Utils.GeometryUtils;
import com.crazytowers.Utils.PositionUtils;
import com.crazytowers.config.ApplicationConfig;
import com.crazytowers.config.DamageType;
import com.crazytowers.data.InAttackRangeContent;
import com.crazytowers.handlers.request.impl.MoveAction;
import com.crazytowers.handlers.response.impl.DeltaResponse;
import com.crazytowers.model.*;
import com.crazytowers.service.ICollisionService;
import com.crazytowers.service.ISaveService;
import com.crazytowers.service.IShowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Random;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Stream;

@Service
public class CollisionServiceImpl implements ICollisionService {
    private static final Random rand = new Random();
    private final ISaveService saveService;
    private final IShowService showService;
    private final GeometryUtils geoUtils;
    private final PositionUtils positionUtils;
    private final ApplicationConfig config;

    @Autowired
    public CollisionServiceImpl(ISaveService saveService, IShowService showService, GeometryUtils geoUtils, PositionUtils positionUtils, ApplicationConfig config) {
        this.saveService = saveService;
        this.showService = showService;
        this.geoUtils = geoUtils;
        this.positionUtils = positionUtils;
        this.config = config;
    }

    @Override
    public DeltaResponse detectCollisionsAndReact(Player movedPlayer, DeltaResponse showableContent, MoveAction action) {
        var isTower = movedPlayer.getTower().isMoving();
        if (isTower) {
            getFurtherFilteredStream(showableContent.getNearbyBricks().parallelStream(), showableContent.getPlayer().getTower())
                    .forEach(y -> detectCollisionsWithBricksAndReact(movedPlayer, y, true));
            getFurtherFilteredStream(showableContent.getNearbyGatherers().parallelStream(), showableContent.getPlayer().getTower())
                    .forEach(y -> detectCollisionsWithLocatablesAndReact(movedPlayer, y, action, true));
            getFurtherFilteredStream(showableContent.getNearbyTowers().parallelStream(), showableContent.getPlayer().getTower())
                    .forEach(y -> detectCollisionsWithLocatablesAndReact(movedPlayer, y, action, true));
        } else {
            getFurtherFilteredStream(showableContent.getNearbyBricks().parallelStream(), showableContent.getPlayer().getGatherer())
                    .forEach(y -> detectCollisionsWithBricksAndReact(movedPlayer, y, false));
            getFurtherFilteredStream(showableContent.getNearbyGatherers().parallelStream(), showableContent.getPlayer().getGatherer())
                    .forEach(y -> detectCollisionsWithLocatablesAndReact(movedPlayer, y, action, false));
            getFurtherFilteredStream(showableContent.getNearbyTowers().parallelStream(), showableContent.getPlayer().getGatherer())
                    .forEach(y -> detectCollisionsWithLocatablesAndReact(movedPlayer, y, action, false));
        }
        return showableContent;
    }

    @Override
    public boolean closeAttack(Player player, DeltaResponse showableContent) {
        getFurtherFilteredStream(showableContent.getNearbyGatherers().parallelStream(), showableContent.getPlayer().getGatherer())
                .forEach(gatherer -> detectCloseAttackToGathererAndReact(player, gatherer));
        getFurtherFilteredStream(showableContent.getNearbyTowers().parallelStream(), showableContent.getPlayer().getGatherer())
                .forEach(tower -> detectCloseAttackToTowerAndReact(player, tower));
        return true;
    }

    @Override
    public boolean brickAttack(int realm, AttackBrick brick, InAttackRangeContent attackableContent) {
        final var hasAttacked = new AtomicBoolean(false);
        getFurtherFilteredStream(attackableContent.getGatherers().parallelStream(), brick).forEach(gatherer -> {
            if (!hasAttacked.get()) {
                var result = detectAttackBrickCollisionAndReact(realm, brick, gatherer);
                if (result) {
                    hasAttacked.set(true);
                    saveService.removeAttackBrick(realm, brick);
                }
            }
        });
        if (!hasAttacked.get())
            getFurtherFilteredStream(attackableContent.getTowers().parallelStream(), brick).forEach(tower -> {
                if (!hasAttacked.get()) {
                    var result = detectAttackBrickCollisionAndReact(brick, tower);
                    if (result) {
                        hasAttacked.set(true);
                        saveService.removeAttackBrick(realm, brick);
                    }
                }
            });
        return hasAttacked.get();
    }

    @Override
    public boolean defend(Player player, DeltaResponse showableContent) {
        if ((config.getStartingBricks() + config.getTowerDefenseCost()) < player.getTower().getBricks()) {
            var auxTower = player.getTower();
            auxTower.setStatus(Status.DEFEND, config.getAnimationDuration());
            auxTower.setBricks(auxTower.getBricks() - config.getTowerDefenseCost());
            getFurtherFilteredStream(showableContent.getNearbyGatherers().parallelStream(), player.getTower())
                    .forEach(gatherer -> detectTowerDefenseToGathererAndReact(player, gatherer));
            return true;
        } else
            return false;
    }

    private <T extends Locatable> Stream<T> getFurtherFilteredStream(Stream<T> stream, Locatable source) {
        return stream.filter(t -> t.getPosX() >= source.getPosX() - config.getFineDetectionFilterDifference() && t.getPosX() <= source
                .getPosX() + config.getFineDetectionFilterDifference() && t.getPosZ() >= source.getPosZ() - config
                .getFineDetectionFilterDifference() && t.getPosZ() <= source.getPosZ() + config.getFineDetectionFilterDifference());
    }

    private void detectTowerDefenseToGathererAndReact(Player player, Gatherer toDetect) {
        if (!toDetect.isInGraceTime() && geoUtils.isInDefenseRange(player.getTower(), toDetect)) {
            calculateNewGathererHealthOrReturnToTower(player.getRealm(), toDetect);
        }
    }

    private void detectCloseAttackToGathererAndReact(Player player, Gatherer toDetect) {
        if (!toDetect.isInGraceTime() && geoUtils.isInAttackRangeAndDirection(player.getGatherer(), toDetect)) {
            calculateNewGathererHealthOrReturnToTower(player.getRealm(), toDetect);
        }
    }

    private void calculateNewGathererHealthOrReturnToTower(int realm, Gatherer toDetect) {
        final var newHealth = toDetect.getHealth() - 1;
        if (newHealth > 0) {
            toDetect.setHealth(newHealth);
            toDetect.setStatus(Status.RECEIVE_DAMAGE, config.getAnimationDuration());
            toDetect.recoverLife(config.getGathererLifeRenewalInMs());
        } else {
            dropBricks(realm, toDetect);
            toDetect.setHealth(Gatherer.FULL_LIFE);
            final var auxTower = showService.getTower(realm, toDetect.getId());
            toDetect.setPosX(auxTower.getPosX());
            toDetect.setPosZ(auxTower.getPosZ());
            toDetect.setTimestampPenalizationTime(System.currentTimeMillis());
            toDetect.setBricks(0);
        }
    }

    private void dropBricks(int realm, Gatherer deadGatherer) {
        var bricksToCreate = deadGatherer.getBricks() / 10;
        for (var i = 0; i < bricksToCreate; i++) {
            var randomPos = geoUtils.getRandomPositionNearPlayer(deadGatherer);
            saveService.saveNewBrick(realm, true, randomPos.getPosX(), randomPos.getPosZ(), true);
        }
    }

    private void detectCloseAttackToTowerAndReact(Player player, Tower toDetect) {
        if (!toDetect.isInGraceTime() && geoUtils.isInAttackRangeAndDirection(player.getGatherer(), toDetect)) {
            var addToAttacker = 0;
            var newHealth = 0;
            if (config.getDamageType() == DamageType.FIXED) {
                newHealth = toDetect.getBricks() - config.getCloseAttackBrickDamage();
                addToAttacker = config.getCloseAttackBrickDamage();
            } else {
                addToAttacker = (int) (toDetect.getBricks() * (config.getPercentDamage() / 100.00));
                addToAttacker = Math.max(addToAttacker, config.getCloseAttackBrickDamage());
                newHealth = toDetect.getBricks() - addToAttacker;
            }
            if (newHealth > 0) {
                toDetect.setStatus(Status.RECEIVE_DAMAGE, config.getAnimationDuration());
                toDetect.setBricks(newHealth);
            } else {
                final var toRemove = showService.searchPlayerByUUID(toDetect.getId());
                saveService.removeWhenDisconnected(toRemove.getSessionId());
            }
            var gatherer = player.getGatherer();
            gatherer.setBricks(gatherer.getBricks() + addToAttacker);
        }
    }

    private boolean detectAttackBrickCollisionAndReact(int realm, AttackBrick brick, Gatherer toDetect) {
        if (!toDetect.isInGraceTime() && geoUtils.isColliding(brick, toDetect)) {
            calculateNewGathererHealthOrReturnToTower(realm, toDetect);
            return true;
        } else
            return false;
    }

    private boolean detectAttackBrickCollisionAndReact(AttackBrick brick, Tower toDetect) {
        if (!toDetect.isInGraceTime() && geoUtils.isColliding(brick, toDetect)) {
            var newHealth = 0;
            if (config.getDamageType() == DamageType.FIXED) {
                newHealth = toDetect.getBricks() - config.getRangeAttackBrickDamage();
            } else {
                var damage = (int) (toDetect.getBricks() * (config.getPercentDamage() / 100.00));
                damage = Math.max(damage, config.getRangeAttackBrickDamage());
                newHealth = toDetect.getBricks() - damage;
            }
            if (newHealth > 0) {
                toDetect.setStatus(Status.RECEIVE_DAMAGE, config.getAnimationDuration());
                toDetect.setBricks(newHealth);
            } else {
                final var toRemove = showService.searchPlayerByUUID(toDetect.getId());
                saveService.removeWhenDisconnected(toRemove.getSessionId());
            }
            return true;
        } else
            return false;
    }

    private boolean isColliding(Player movedPlayer, Locatable toDetect, boolean tower) {
        return tower ? geoUtils.isColliding(movedPlayer.getTower(), toDetect) : geoUtils
                .isColliding(movedPlayer.getGatherer(), toDetect);
    }

    private void detectCollisionsWithBricksAndReact(Player movedPlayer, Brick toDetect, boolean tower) {
        var isColliding = isColliding(movedPlayer, toDetect, tower);
        if (isColliding) {
            saveService.removeBrick(movedPlayer.getRealm(), toDetect);
            var pos = positionUtils.getRandomPositionInMap();
            if (!toDetect.isDropped())
                saveService.saveNewBrick(movedPlayer.getRealm(), rand.nextBoolean(), pos.getPosX(), pos.getPosZ(), false);
            if (tower)
                movedPlayer.getTower().setBricks(movedPlayer.getTower().getBricks() + (toDetect.isLarge() ? 10 : 5));
            else
                movedPlayer.getGatherer().setBricks(movedPlayer.getGatherer().getBricks() + (toDetect.isLarge() ? 10 : 5));
        }
    }

    private void detectCollisionsWithLocatablesAndReact(Player movedPlayer, Locatable toDetect, MoveAction action, boolean tower) {
        var isColliding = isColliding(movedPlayer, toDetect, tower);
        if (isColliding) {
            Locatable movingObject = tower ? movedPlayer.getTower() : movedPlayer.getGatherer();
            var newPos = geoUtils.calculatePositionAvoidingObject(movingObject, toDetect, action);
            if (tower) {
                movedPlayer.getTower().setPosZ(newPos.getPosZ());
                movedPlayer.getTower().setPosX(newPos.getPosX());
            } else {
                movedPlayer.getGatherer().setPosZ(newPos.getPosZ());
                movedPlayer.getGatherer().setPosX(newPos.getPosX());
            }
        }
    }
}

package com.crazytowers.service.impl;

import com.crazytowers.Utils.auxiliary.NumberUtils;
import com.crazytowers.config.ApplicationConfig;
import com.crazytowers.dao.IEntityCacheDAO;
import com.crazytowers.model.*;
import com.crazytowers.service.ISaveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class SaveServiceImpl implements ISaveService {

    private final ApplicationConfig config;
    private final IEntityCacheDAO entityCacheDAO;
    private final NumberUtils numberUtils;

    @Autowired
    public SaveServiceImpl(ApplicationConfig config, IEntityCacheDAO entityCacheDAO, NumberUtils numberUtils) {
        this.config = config;
        this.entityCacheDAO = entityCacheDAO;
        this.numberUtils = numberUtils;
    }

    @Override
    public Gatherer saveNewGatherer(Player player, double posX, double posZ) {
        Gatherer newGatherer = new Gatherer(player.getId(), player.getName(), posX, posZ, 1, 0);
        entityCacheDAO.save(player.getRealm(), newGatherer);
        return newGatherer;
    }

    @Override
    public Tower saveNewTower(Player player, double posX, double posZ) {
        Tower newTower = new Tower(player.getId(), player.getName(), posX, posZ, 0, 0, config.getStartingBricks());
        entityCacheDAO.save(player.getRealm(), newTower);
        return newTower;
    }

    @Override
    public Brick saveNewBrick(int realm, boolean large, double posX, double posZ, boolean dropped) {
        Brick newBrick = new Brick(UUID.randomUUID(), large, posX, posZ, numberUtils.getRandomAngleInRadians(), dropped);
        entityCacheDAO.save(realm, newBrick);
        return newBrick;
    }

    @Override
    public AttackBrick saveNewAttackBrick(int realm, double posX, double posZ, double heading, double speed, double distanceToCover) {
        var newBrick = new AttackBrick(UUID.randomUUID(), posX, posZ, heading, speed, distanceToCover);
        entityCacheDAO.save(realm, newBrick);
        return newBrick;
    }

    @Override
    public void saveNewPlayer(Player player) {
        entityCacheDAO.save(player);
    }

    @Override
    public void removeWhenDisconnected(String sessionId) {
        final var removed = entityCacheDAO.removePlayer(sessionId);
        entityCacheDAO.removeTower(removed.getRealm(), removed.getId());
        entityCacheDAO.removeGatherer(removed.getRealm(), removed.getId());
    }

    @Override
    public void removeBrick(int realm, Brick toRemove) {
        entityCacheDAO.removeBrick(realm, toRemove.getId());
    }

    @Override
    public void removeAttackBrick(int realm, AttackBrick toRemove) {
        entityCacheDAO.removeAttackBrick(realm, toRemove.getId());
    }
}

package com.crazytowers.service.impl;

import com.crazytowers.Utils.GeometryUtils;
import com.crazytowers.Utils.auxiliary.Position;
import com.crazytowers.config.ApplicationConfig;
import com.crazytowers.handlers.Actions;
import com.crazytowers.handlers.request.impl.MoveAction;
import com.crazytowers.handlers.response.BaseResponse;
import com.crazytowers.handlers.response.impl.ConnectResponse;
import com.crazytowers.handlers.response.impl.DeltaResponse;
import com.crazytowers.handlers.response.impl.RankingResponse;
import com.crazytowers.model.Player;
import com.crazytowers.model.Status;
import com.crazytowers.service.ICollisionService;
import com.crazytowers.service.IProcessActionService;
import com.crazytowers.service.ISaveService;
import com.crazytowers.service.IShowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.Disposable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.time.Duration;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class ProcessActionServiceImpl implements IProcessActionService {
    private static final Map<String, Disposable> moveMap = new ConcurrentHashMap<>();
    private final ApplicationConfig appConfig;
    private final IShowService showService;
    private final GeometryUtils geoUtils;
    private final ICollisionService collService;
    private final ISaveService saveService;

    @Autowired
    public ProcessActionServiceImpl(ApplicationConfig appConfig, IShowService showService, GeometryUtils geoUtils, ICollisionService collService, ISaveService saveService) {
        this.appConfig = appConfig;
        this.showService = showService;
        this.geoUtils = geoUtils;
        this.collService = collService;
        this.saveService = saveService;
    }

    @Override
    public Flux<DeltaResponse> getDeltaIntervalFlux(final String sessionId) {
        return Flux.interval(Duration.ofMillis(appConfig.getPositionRefreshIntervalInMs())).flatMap(aLong -> getDelta(sessionId));
    }

    @Override
    public Flux<RankingResponse> getRankingInterval(Player player) {
        return Flux.interval(Duration.ofMillis(appConfig.getRankingRefreshInMs())).flatMap(aLong -> showService.getRankingInterval(player));
    }

    @Override
    public void removeMoveIntervalFlux(String sessionId) {
        if (moveMap.containsKey(sessionId)) {
            var disposable = moveMap.get(sessionId);
            disposable.dispose();
            moveMap.remove(sessionId);
        }
    }

    @Override
    public Disposable registerMoveIntervalFlux(final Player player) {
        return Flux.interval(Duration.ofMillis(appConfig.getPositionRefreshIntervalInMs())).flatMap(aLong -> {
            if ((player.isMobile() && !player.isStopMoving()) || (!player.isMobile() && !geoUtils
                    .hasReachedGoal(player.getGatherer().isActive() ? player.getGatherer() : player.getTower(), player.getMoveAction()))) {
                if (player.getGatherer().isActive()) {
                    player.getGatherer().setStatus(Status.MOVE, appConfig.getAnimationDuration());
                } else {
                    player.getTower().setStatus(Status.MOVE, appConfig.getAnimationDuration());
                }
                var modifiedPlayer = positionCalculate(player);
                return showService.getShowableContent(modifiedPlayer)
                        .doOnNext(x -> collService.detectCollisionsAndReact(modifiedPlayer, x, player.getMoveAction())).then();
            } else
                return Mono.empty();
        }).parallel().runOn(Schedulers.boundedElastic()).subscribe();
    }

    @Override
    public Disposable registerRangeAttackIntervalFlux(final Player player) {
        final var gatherer = player.getGatherer();
        final var attackBrick = saveService
                .saveNewAttackBrick(player.getRealm(), gatherer.getPosX(), gatherer.getPosZ(), gatherer.getHeading(), gatherer
                        .getSpeed() * 1.5, geoUtils.getAttackDistanceToCover(player));
        return Flux.interval(Duration.ofMillis(appConfig.getPositionRefreshIntervalInMs())).flatMap(aLong -> {
            var position = geoUtils.calculateNewPosition(attackBrick);
            var coveredDistance = geoUtils
                    .findDistance(attackBrick.getPosX(), attackBrick.getPosZ(), position.getPosX(), position.getPosZ());
            attackBrick.setPosX(position.getPosX());
            attackBrick.setPosZ(position.getPosZ());
            attackBrick.setDistanceToCover(attackBrick.getDistanceToCover() - coveredDistance);
            return showService.getInAttackRangeContent(player, attackBrick)
                    .map(x -> collService.brickAttack(player.getRealm(), attackBrick, x));
        }).takeUntil(hasCollided -> hasCollided || geoUtils.attackBrickShouldContinue(attackBrick))
                .doOnTerminate(() -> saveService.removeAttackBrick(player.getRealm(), attackBrick)).parallel().runOn(Schedulers.boundedElastic())
                .subscribe();
    }

    private Player positionCalculate(Player player) {
        if (player.getGatherer().getTimestampPenalizationTime() < 0 || player.getGatherer().getTimestampPenalizationTime() + appConfig
                .getGathererPenalizationInMs() < System.currentTimeMillis()) {
            Position newPos = geoUtils.calculateNewPosition(player, player.getMoveAction());
            var gatherer = player.getGatherer();
            if (gatherer.isActive()) {
                gatherer.setPosX(newPos.getPosX());
                gatherer.setPosZ(newPos.getPosZ());
                gatherer.setHeading(newPos.getHeading());
                gatherer.setTimestampPenalizationTime(-1);
                player.setGatherer(gatherer);
            } else {
                var tower = player.getTower();
                tower.setPosX(newPos.getPosX());
                tower.setPosZ(newPos.getPosZ());
                tower.setHeading(newPos.getHeading());
                player.setTower(tower);
            }
            return player;
        } else
            return null;
    }

    private boolean canMove(Player player) {
        return player.getGatherer().getTimestampPenalizationTime() < 0 || player.getGatherer().getTimestampPenalizationTime() + appConfig
                .getGathererPenalizationInMs() < System.currentTimeMillis();
    }

    @Override
    public Mono<BaseResponse> move(Player player, MoveAction moveAction) {
        setLastActivity(player);
        if (canMove(player)) {
            player.setStopMoving(false);
            player.setMoveAction(moveAction);
            if (!moveMap.containsKey(player.getSessionId())) {
                var intervalFlux = registerMoveIntervalFlux(player);
                moveMap.put(player.getSessionId(), intervalFlux);
            }
            return Mono.just(new BaseResponse(Actions.MOVE, true));
        } else
            return Mono.just(new BaseResponse(Actions.MOVE, false));
    }

    @Override
    public Mono<BaseResponse> stopMove(Player player) {
        setLastActivity(player);
        player.setStopMoving(true);
        return Mono.just(new BaseResponse(Actions.STOP_MOVE, true));
    }

    @Override
    public Mono<BaseResponse> closeAttack(Player player) {
        setLastActivity(player);
        var gatherer = player.getGatherer();
        if (gatherer.isActive() && gatherer.getLastCloseAttack() + appConfig.getAttackCadenceInMs() < System.currentTimeMillis()) {
            gatherer.setStatus(Status.CLOSE_ATTACK, appConfig.getAnimationDuration());
            gatherer.setLastCloseAttack(System.currentTimeMillis());
            return showService.getShowableContent(player)
                    .map(deltaResponse -> new BaseResponse(Actions.CLOSE_ATTACK, collService.closeAttack(player, deltaResponse)));
        } else
            return Mono.just(new BaseResponse(Actions.CLOSE_ATTACK, false));
    }

    @Override
    public Mono<BaseResponse> rangeAttack(Player player) {
        setLastActivity(player);
        var gatherer = player.getGatherer();
        if (gatherer.isActive() && gatherer.getLastRangeAttack() + appConfig.getAttackCadenceInMs() < System.currentTimeMillis()) {
            if (player.getGatherer().getBricks() >= appConfig.getRangeAttackCost()) {
                gatherer.setStatus(Status.RANGE_ATTACK, appConfig.getAnimationDuration());
                var auxGatherer = player.getGatherer();
                auxGatherer.setBricks(auxGatherer.getBricks() - appConfig.getRangeAttackCost());
                registerRangeAttackIntervalFlux(player);
                gatherer.setLastRangeAttack(System.currentTimeMillis());
                return Mono.just(new BaseResponse(Actions.RANGE_ATTACK, true));
            } else
                return Mono.just(new BaseResponse(Actions.RANGE_ATTACK, false));
        } else
            return Mono.just(new BaseResponse(Actions.RANGE_ATTACK, false));
    }

    @Override
    public Mono<BaseResponse> defend(Player player) {
        setLastActivity(player);
        if (!player.getTower().isMoving()) {
            return showService.getShowableContent(player)
                    .map(deltaResponse -> new BaseResponse(Actions.DEFEND, collService.defend(player, deltaResponse)));
        } else
            return Mono.just(new BaseResponse(Actions.DEFEND, false));
    }

    @Override
    public Mono<BaseResponse> toggleMoveTower(Player player) {
        setLastActivity(player);
        var tower = player.getTower();
        tower.setMoving(!tower.isMoving());
        var gatherer = player.getGatherer();
        gatherer.setActive(!gatherer.isActive());
        if (gatherer.isActive()) {
            gatherer.setTimestampPenalizationTime(System.currentTimeMillis());
            gatherer.setPosZ(tower.getPosZ());
            gatherer.setPosX(tower.getPosX());
        } else {
            tower.setBricks(tower.getBricks() + gatherer.getBricks());
            gatherer.setBricks(0);
        }
        return Mono.just(new BaseResponse(Actions.TOGGLE_MOVE_TOWER, true));
    }

    @Override
    public Mono<ConnectResponse> getConnectResponse(Player player) {
        setLastActivity(player);
        ConnectResponse toReturn = new ConnectResponse(appConfig.getMapSideSize(), appConfig.getPositionRefreshIntervalInMs(), appConfig
                .getGathererPenalizationInMs(), appConfig.getGraceTimeInMs(), player);
        return Mono.just(toReturn);
    }

    @Override
    public Mono<DeltaResponse> getDelta(String sessionId) {
        var player = showService.getPlayer(sessionId);
        if (player != null)
            return showService.getShowableContent(player);
        else
            return Mono.error(new Exception("The player is dead"));
    }

    private void setLastActivity(Player player) {
        player.setLastActivity(System.currentTimeMillis());
    }
}

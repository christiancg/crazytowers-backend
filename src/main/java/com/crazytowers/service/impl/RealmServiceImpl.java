package com.crazytowers.service.impl;

import com.crazytowers.Utils.PositionUtils;
import com.crazytowers.config.ApplicationConfig;
import com.crazytowers.dao.IEntityCacheDAO;
import com.crazytowers.service.IRealmService;
import com.crazytowers.service.ISaveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Random;
import java.util.stream.IntStream;

@Service
public class RealmServiceImpl implements IRealmService {

    private final PositionUtils posService;
    private final ISaveService saveService;
    private final ApplicationConfig config;
    private final IEntityCacheDAO entityCacheDAO;

    private static final Random rand = new Random();

    @Autowired
    public RealmServiceImpl(PositionUtils posService, ISaveService saveService, ApplicationConfig config, IEntityCacheDAO entityCacheDAO) {
        this.posService = posService;
        this.saveService = saveService;
        this.config = config;
        this.entityCacheDAO = entityCacheDAO;
    }

    @Override
    public int getAvailableRealm() {
        int realm = entityCacheDAO.getAvailableRealm();
        if (realm >= 0) {
            if (!entityCacheDAO.hasBricks(realm)) {
                IntStream.range(0, config.getTotalBricksOnRealm()).parallel().forEach(x -> {
                    var large = rand.nextBoolean();
                    var pos = posService.getRandomPositionInMap();
                    saveService.saveNewBrick(realm, large, pos.getPosX(), pos.getPosZ(), false);
                });
            }
        }
        return realm;
    }
}

package com.crazytowers.dao;

import com.crazytowers.data.PlayerRankingEntry;
import com.crazytowers.model.*;
import reactor.core.publisher.Flux;

import java.util.UUID;

public interface IEntityCacheDAO {
    Player getPlayer(String sessionId);

    Player getPlayer(UUID id);

    Boolean save(Player toSave);

    Tower getTower(int realm, UUID playerId);

    Gatherer getGatherer(int realm, UUID playerId);

    Flux<Tower> getNearbyTowers(int realm, double posX, double posY, int height, UUID exclude);

    Boolean save(int realm, Tower toSave);

    Flux<Gatherer> getNearbyGatherers(int realm, double posX, double posY, int height, UUID exclude);

    Boolean save(int realm, Gatherer toSave);

    Boolean removeTower(int realm, UUID playerId);

    Boolean removeGatherer(int realm, UUID playerId);

    Player removePlayer(String sessionId);

    Flux<Brick> getNearbyBricks(int realm, double posX, double posY, int height);

    Flux<AttackBrick> getNearbyAttackBricks(int realm, double posX, double posY, int height);

    Boolean save(int realm, Brick toSave);

    Boolean removeBrick(int realm, UUID brickId);

    Boolean save(int realm, AttackBrick toSave);

    Boolean removeAttackBrick(int realm, UUID brickId);

    int getAvailableRealm();

    boolean hasBricks(int realm);

    Flux<PlayerRankingEntry> getRankingList(int realm, UUID playerId);
}

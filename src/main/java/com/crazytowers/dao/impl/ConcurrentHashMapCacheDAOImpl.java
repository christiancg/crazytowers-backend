package com.crazytowers.dao.impl;

import com.crazytowers.Utils.GeometryUtils;
import com.crazytowers.config.ApplicationConfig;
import com.crazytowers.dao.IEntityCacheDAO;
import com.crazytowers.data.PlayerRankingEntry;
import com.crazytowers.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;

import java.util.Comparator;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

@Component("concurrentHashMapCache")
public class ConcurrentHashMapCacheDAOImpl implements IEntityCacheDAO {

    private static final Map<String, Player> playerMap = new ConcurrentHashMap<>();
    private static final Map<Integer, Map<UUID, Tower>> towerMap = new ConcurrentHashMap<>();
    private static final Map<Integer, Map<UUID, Gatherer>> gathererMap = new ConcurrentHashMap<>();
    private static final Map<Integer, Map<UUID, Brick>> brickMap = new ConcurrentHashMap<>();
    private static final Map<Integer, Map<UUID, AttackBrick>> attackBrickMap = new ConcurrentHashMap<>();
    private final ApplicationConfig config;

    @Autowired
    public ConcurrentHashMapCacheDAOImpl(ApplicationConfig config) {
        this.config = config;
    }

    @Override
    public Player getPlayer(String sessionId) {
        return playerMap.get(sessionId);
    }

    @Override
    public Player getPlayer(UUID id) {
        return playerMap.values().stream().filter(stringPlayerEntry -> stringPlayerEntry.getId().equals(id)).findFirst().orElse(null);
    }

    @Override
    public Boolean save(Player toSave) {
        synchronized (playerMap) {
            playerMap.put(toSave.getSessionId(), toSave);
        }
        return true;
    }

    @Override
    public Tower getTower(int realm, UUID playerId) {
        return towerMap.get(realm).get(playerId);
    }

    @Override
    public Gatherer getGatherer(int realm, UUID playerId) {
        return gathererMap.get(realm).get(playerId);
    }

    @Override
    public Flux<Tower> getNearbyTowers(int realm, double posX, double posY, int height, UUID exclude) {
        return Flux.fromStream(towerMap.get(realm).entrySet().stream()
                .filter(x -> GeometryUtils.isVisible(posX, posY, height, x.getValue()) && x.getKey() != exclude)
                .map(Map.Entry::getValue));
    }

    @Override
    public Boolean save(int realm, Tower toSave) {
        synchronized (towerMap) {
            if (!towerMap.containsKey(realm))
                towerMap.put(realm, new ConcurrentHashMap<>());
            towerMap.get(realm).put(toSave.getId(), toSave);
        }
        return true;
    }

    @Override
    public Flux<Gatherer> getNearbyGatherers(int realm, double posX, double posY, int height, UUID exclude) {
        return Flux.fromStream(gathererMap.get(realm).entrySet().stream()
                .filter(x -> x.getValue().getTimestampPenalizationTime() < 0 && GeometryUtils
                        .isVisible(posX, posY, height, x.getValue()) && x.getKey() != exclude && x.getValue().isActive())
                .map(Map.Entry::getValue));
    }

    @Override
    public Flux<Brick> getNearbyBricks(int realm, double posX, double posY, int height) {
        return Flux.fromStream(brickMap.get(realm).values().stream().filter(x -> GeometryUtils.isVisible(posX, posY, height, x)));
    }

    @Override
    public Flux<AttackBrick> getNearbyAttackBricks(int realm, double posX, double posY, int height) {
        return Flux.fromStream(!attackBrickMap.isEmpty() ? attackBrickMap.get(realm).values().stream()
                .filter(x -> GeometryUtils.isVisible(posX, posY, height, x)) : Stream.empty());
    }

    @Override
    public Boolean save(int realm, Gatherer toSave) {
        synchronized (gathererMap) {
            if (!gathererMap.containsKey(realm))
                gathererMap.put(realm, new ConcurrentHashMap<>());
            gathererMap.get(realm).put(toSave.getId(), toSave);
        }
        return true;
    }

    @Override
    public Boolean removeTower(int realm, UUID playerId) {
        synchronized (towerMap) {
            towerMap.get(realm).remove(playerId);
        }
        return true;
    }

    @Override
    public Boolean removeGatherer(int realm, UUID playerId) {
        synchronized (gathererMap) {
            gathererMap.get(realm).remove(playerId);
        }
        return true;
    }

    @Override
    public Player removePlayer(String sessionId) {
        Player toReturn;
        synchronized (playerMap) {
            toReturn = playerMap.remove(sessionId);
        }
        return toReturn;
    }

    @Override
    public Boolean save(int realm, Brick toSave) {
        synchronized (brickMap) {
            if (!brickMap.containsKey(realm))
                brickMap.put(realm, new ConcurrentHashMap<>());
            brickMap.get(realm).put(toSave.getId(), toSave);
        }
        return true;
    }

    @Override
    public Boolean removeBrick(int realm, UUID brickId) {
        synchronized (brickMap) {
            brickMap.get(realm).remove(brickId);
        }
        return true;
    }

    @Override
    public Boolean save(int realm, AttackBrick toSave) {
        synchronized (attackBrickMap) {
            if (!attackBrickMap.containsKey(realm))
                attackBrickMap.put(realm, new ConcurrentHashMap<>());
            attackBrickMap.get(realm).put(toSave.getId(), toSave);
        }
        return true;
    }

    @Override
    public Boolean removeAttackBrick(int realm, UUID brickId) {
        synchronized (attackBrickMap) {
            attackBrickMap.get(realm).remove(brickId);
        }
        return true;
    }

    @Override
    public int getAvailableRealm() {
        if (towerMap.isEmpty()) {
            return 0;
        } else {
            var auxRealm = towerMap.entrySet().stream().filter(x -> x.getValue().keySet().size() < config.getMaxPlayersPerRealm())
                    .findFirst();
            if (auxRealm.isPresent())
                return auxRealm.get().getKey();
            else if (towerMap.size() < config.getMaxRealms())
                return towerMap.size() + 1;
            else
                return -1;
        }
    }

    @Override
    public boolean hasBricks(int realm) {
        if (towerMap.containsKey(realm)) {
            return brickMap.get(realm).size() > 0;
        } else
            return false;
    }

    @Override
    public Flux<PlayerRankingEntry> getRankingList(int realm, UUID playerId) {
        var ai = new AtomicInteger(1);
        return Flux.fromStream(towerMap.get(realm).entrySet().stream()
                .map(uuidTowerEntry -> new PlayerRankingEntry(uuidTowerEntry.getKey(), uuidTowerEntry.getValue().getName(), uuidTowerEntry
                        .getValue().getBricks())).sorted(Comparator.comparingInt(PlayerRankingEntry::getBricks).reversed())
                .peek(playerRankingEntry -> {
                    playerRankingEntry.setPosition(ai.get());
                    ai.set(ai.get() + 1);
                }).filter(playerRankingEntry -> playerRankingEntry.getPosition() <= config.getRankingListLength() || playerRankingEntry
                        .getId().equals(playerId)));
    }
}

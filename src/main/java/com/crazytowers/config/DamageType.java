package com.crazytowers.config;


public enum DamageType {
    FIXED, PERCENT;
}

package com.crazytowers.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "crazy-towers")
@Data
public class ApplicationConfig {
    private int mapSideSize;
    private int maxRealms;
    private int maxPlayersPerRealm;
    private double bricksPerPlayer;
    private int minPlayerRefreshTimeInMs;
    private int startingBricks;
    private DamageType damageType;
    private int closeAttackBrickDamage;
    private int rangeAttackBrickDamage;
    private double percentDamage;
    private int fineDetectionFilterDifference;
    private int positionRefreshIntervalInMs;
    private int gathererPenalizationInMs;
    private int towerDefenseCost;
    private int rangeAttackCost;
    private double rangeAttackDistanceBricksRelation;
    private int attackCadenceInMs;
    private int gathererLifeRenewalInMs;
    private int graceTimeInMs;
    private int rankingListLength;
    private int rankingRefreshInMs;
    private int sessionTimeoutInMs;
    private int animationDuration;

    public int getTotalBricksOnRealm() {
        return (int) (maxPlayersPerRealm * bricksPerPlayer);
    }
}

package com.crazytowers.model;

import com.crazytowers.Utils.GsonExclude;
import com.crazytowers.handlers.request.impl.MoveAction;
import lombok.Data;

import java.util.UUID;

@Data
public class Player implements Cloneable {
    private String sessionId;
    private UUID id;
    @GsonExclude
    private boolean isMobile;
    private String name;
    private int realm;
    private Tower tower;
    private Gatherer gatherer;
    @GsonExclude
    private long lastActivity;
    @GsonExclude
    private MoveAction moveAction;
    @GsonExclude
    private boolean stopMoving;
    private int maximumVisibleRange;

    private Player() {
    }

    public Player(String sessionId, String name, boolean isMobile) {
        new Player();
        this.sessionId = sessionId;
        this.name = name;
        this.isMobile = isMobile;
        this.realm = -1;
    }

    public Player(String sessionId, UUID id, String name, int realm) {
        this.sessionId = sessionId;
        this.id = id;
        this.name = name;
        this.realm = realm;
    }

    private Player(String sessionId, UUID id, String name, int realm, Tower tower, Gatherer gatherer) {
        this.sessionId = sessionId;
        this.id = id;
        this.name = name;
        this.realm = realm;
        this.tower = tower;
        this.gatherer = gatherer;
    }

    @Override
    public Object clone() {
        Player player;
        try {
            player = (Player) super.clone();
        } catch (CloneNotSupportedException e) {
            player = new Player(this.getSessionId(),this.getId(),this.getName(),this.getRealm(), this.getTower(),this.getGatherer());
        }
		player.tower = (Tower) this.tower.clone();
		player.gatherer = (Gatherer) this.gatherer.clone();
		return player;
    }
}

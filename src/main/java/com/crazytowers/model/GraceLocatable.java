package com.crazytowers.model;

import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Setter;
import reactor.core.publisher.Flux;

import java.time.Duration;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Data
public abstract class GraceLocatable extends Locatable {
    @Setter(AccessLevel.NONE)
    private boolean inGraceTime;

    protected GraceLocatable(UUID id, String name, double posX, double posZ, double speed, double heading, ShapeType shapeType, int width, int height) {
        super(id, name, posX, posZ, speed, heading, shapeType, width, height);
    }

    public void setUpGraceTime(long millisecondsGraceTime) {
        inGraceTime = true;
        Flux.interval(Duration.ofMillis(millisecondsGraceTime)).take(1).subscribe(aLong -> inGraceTime = false);
    }
}

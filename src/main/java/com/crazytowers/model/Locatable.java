package com.crazytowers.model;

import com.crazytowers.Utils.GsonExclude;
import lombok.Data;

import java.util.UUID;

@Data
public abstract class Locatable implements Cloneable {
    double posX;
    double posZ;
    private double speed;
    private double heading;
    private UUID id;
    private String name;
    @GsonExclude
    private ShapeType shapeType;
    @GsonExclude
    private int width;
    @GsonExclude
    private int height;

    Locatable(UUID id, String name, double posX, double posZ, double speed, double heading, ShapeType shapeType, int width, int height) {
        this.id = id;
        this.name = name;
        this.posX = posX;
        this.posZ = posZ;
        this.speed = speed;
        this.heading = heading;
        this.shapeType = shapeType;
        this.width = width;
        this.height = height;
    }

    @Override
    public Object clone() {
        Locatable locatable;
        try {
            locatable = (Locatable) super.clone();
        } catch (CloneNotSupportedException e) {
            locatable = null;
        }
        return locatable;
    }
}

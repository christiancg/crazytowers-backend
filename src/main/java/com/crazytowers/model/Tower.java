package com.crazytowers.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Data
public class Tower extends StatableGraceable {
    private final static int WIDTH = 15;
    private final static int HEIGHT = 15;
    private int bricks;
    private boolean isMoving;

    public Tower(UUID id, String name, double posX, double posZ, double speed, double heading, int bricks) {
        super(id, name, posX, posZ, speed, heading, ShapeType.RECTANGLE, WIDTH, HEIGHT);
        this.bricks = bricks;
    }
}

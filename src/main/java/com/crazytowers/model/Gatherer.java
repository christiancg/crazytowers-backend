package com.crazytowers.model;

import com.crazytowers.Utils.GsonExclude;
import lombok.Data;
import lombok.EqualsAndHashCode;
import reactor.core.publisher.Flux;

import java.time.Duration;
import java.util.UUID;


@EqualsAndHashCode(callSuper = true)
@Data
public class Gatherer extends StatableGraceable {
    private final static int WIDTH = 10;
    private final static int HEIGHT = 10;
    public static int FULL_LIFE = 3;

    private int health;
    private long timestampPenalizationTime;
    private boolean active;
    private int bricks;
    @GsonExclude
    private long lastCloseAttack;
    @GsonExclude
    private long lastRangeAttack;
    @GsonExclude
    private boolean isRecoveringLife;

    public Gatherer(UUID id, String name, double posX, double posZ, double speed, double heading) {
        super(id, name, posX, posZ, speed, heading, ShapeType.CIRCLE, WIDTH, HEIGHT);
        this.health = FULL_LIFE;
        this.timestampPenalizationTime = -1;
        this.active = true;
        this.bricks = 0;
        this.lastCloseAttack = -1;
        this.lastRangeAttack = -1;
    }

    public void recoverLife(long millisecondsForRecovery) {
        if (!isRecoveringLife) {
            isRecoveringLife = true;
            Flux.interval(Duration.ofMillis(millisecondsForRecovery)).takeWhile(aLong -> health < FULL_LIFE).subscribe(aLong -> {
                health = health + 1;
                if (health == FULL_LIFE)
                    isRecoveringLife = false;
            });
        }
    }
}

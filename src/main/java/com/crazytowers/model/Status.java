package com.crazytowers.model;

public enum Status {
    MOVE, CLOSE_ATTACK, RANGE_ATTACK, RECEIVE_DAMAGE, DEFEND
}

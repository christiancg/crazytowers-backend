package com.crazytowers.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Data
public class Brick extends Locatable {
    private final static int WIDTH_LARGE = 6;
    private final static int HEIGHT_LARGE = 3;
    private final static int WIDTH_SMALL = 4;
    private final static int HEIGHT_SMALL = 2;

    private boolean large;
    private boolean dropped;

    public Brick(UUID id, Boolean large, double posX, double posZ, double heading, boolean dropped) {
        super(id, null, posX, posZ, 0, heading, ShapeType.RECTANGLE, large ? WIDTH_LARGE : WIDTH_SMALL, large ? HEIGHT_LARGE : HEIGHT_SMALL);
        this.large = large;
        this.dropped = dropped;
    }
}

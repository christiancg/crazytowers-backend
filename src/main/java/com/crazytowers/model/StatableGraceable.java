package com.crazytowers.model;

import com.crazytowers.Utils.GsonExclude;
import lombok.*;
import reactor.core.publisher.Flux;

import java.time.Duration;
import java.util.*;

@EqualsAndHashCode(callSuper = true)
@Data
public abstract class StatableGraceable extends GraceLocatable {

    @GsonExclude
    @Setter(AccessLevel.NONE)
    @Getter(AccessLevel.NONE)
    Map<Status, Long> statusStart = new HashMap<>();
    @Setter(AccessLevel.NONE)
    Set<Status> statusList = new HashSet<>();

    protected StatableGraceable(UUID id, String name, double posX, double posZ, double speed, double heading, ShapeType shapeType, int width, int height) {
        super(id, name, posX, posZ, speed, heading, shapeType, width, height);
    }

    public void setStatus(Status status, int durationInMs) {
        if (!statusStart.containsKey(status)) {
            Flux.interval(Duration.ofMillis(durationInMs)).takeWhile(aLong -> statusStart.containsKey(status)).subscribe(aLong -> {
                var s = statusStart.get(status);
                if (s != null) {
                    if (s + durationInMs < System.currentTimeMillis()) {
                        statusStart.remove(status);
                        statusList.remove(status);
                    }
                }
            });
            statusList.add(status);
        }
        statusStart.put(status, System.currentTimeMillis());
    }
}

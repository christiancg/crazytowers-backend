package com.crazytowers.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Data
public class AttackBrick extends Locatable {
    private final static int WIDTH_LARGE = 6;
    private final static int HEIGHT_LARGE = 3;

    private double distanceToCover;

    public AttackBrick(UUID id, double posX, double posZ, double heading, double speed, double distanceToCover) {
        super(id, null, posX, posZ, speed, heading, ShapeType.RECTANGLE, WIDTH_LARGE, HEIGHT_LARGE);
        this.distanceToCover = distanceToCover;
    }
}

package com.crazytowers.model;

public enum ShapeType {
    CIRCLE, RECTANGLE
}

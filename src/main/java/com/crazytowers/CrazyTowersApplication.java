package com.crazytowers;

import com.crazytowers.config.ApplicationConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties(ApplicationConfig.class)
public class CrazyTowersApplication {
	public static void main(String[] args) {
		SpringApplication.run(CrazyTowersApplication.class, args);
	}
}

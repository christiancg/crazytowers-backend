package com.crazytowers.Utils;

import com.crazytowers.Utils.auxiliary.NumberUtils;
import com.crazytowers.Utils.auxiliary.Position;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PositionUtils {

    private final NumberUtils numberUtils;

    @Autowired
    public PositionUtils(NumberUtils numberUtils) {
        this.numberUtils = numberUtils;
    }

    public Position getRandomPositionInMap() {
        double posX = numberUtils.getRandomInMap();
        double posZ = numberUtils.getRandomInMap();
        return new Position(posX, posZ, 0);
    }
}

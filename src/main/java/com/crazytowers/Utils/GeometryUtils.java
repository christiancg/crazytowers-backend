package com.crazytowers.Utils;

import com.crazytowers.Utils.auxiliary.Position;
import com.crazytowers.config.ApplicationConfig;
import com.crazytowers.handlers.request.impl.MoveAction;
import com.crazytowers.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GeometryUtils {
    private static final double CLOSE_ATTACK_RADIUS_RADIANS = 1.5;

    private final ApplicationConfig config;

    @Autowired
    public GeometryUtils(ApplicationConfig config) {
        this.config = config;
    }

    public static boolean isVisible(double posX, double posY, int height, Locatable toCheck) {
        return Math.hypot(Math.abs(toCheck.getPosZ() - posY), Math.abs(toCheck.getPosX() - posX)) < height;
    }

    public int getMaximumRangeForHeight(int height) {
        return (int) Math.max(height / 2.0, config.getStartingBricks() * 2.0);
    }

    public Position calculateNewPosition(Player player, MoveAction action) {
        var gatherer = player.getGatherer();
        var tower = player.getTower();
        var heading = action.getHeading();
        var maxVisibleRange = player.getMaximumVisibleRange();
        Position pos;
        if (gatherer.isActive()) {
            if (!player.isMobile())
                heading = calculateHeading(gatherer.getPosX(), gatherer.getPosZ(), action.getPosX(), action.getPosZ());
            pos = calculatePosition(gatherer, tower, gatherer.getSpeed(), heading, maxVisibleRange);
        } else {
            if (!player.isMobile())
                heading = calculateHeading(tower.getPosX(), tower.getPosZ(), action.getPosX(), action.getPosZ());
            double speed;
            var penalization = tower.getBricks() / (tower.getBricks() + Math.pow(Math.log(tower.getBricks()), 3.5));
            if (penalization > 75)
                speed = gatherer.getSpeed() * 0.25;
            else
                speed = gatherer.getSpeed() - penalization;
            pos = calculatePosition(tower, tower, speed, heading, maxVisibleRange);
        }
        return pos;
    }

    public Position calculateNewPosition(AttackBrick brick) {
        double newX = brick.getPosX() + brick.getSpeed() * Math.cos(brick.getHeading());
        double newZ = brick.getPosZ() + brick.getSpeed() * Math.sin(brick.getHeading());
        return new Position(newX, newZ, brick.getHeading());
    }

    private Position calculatePosition(Locatable movingObj, Tower tower, double speed, double heading, int maximumVisibleRange) {
        double headingConstraintX = Math.cos(heading);
        double newX = movingObj.getPosX() + speed * headingConstraintX;
        newX = inMap(newX);
        double headingConstraintZ = Math.sin(heading);
        double newZ = movingObj.getPosZ() + speed * headingConstraintZ;
        newZ = inMap(newZ);
        if (!GeometryUtils.isVisible(newX, newZ, maximumVisibleRange, tower)) {
            double vX = newX - tower.getPosX();
            double vY = newZ - tower.getPosZ();
            double magV = Math.sqrt(vX*vX + vY*vY);
            newX = tower.getPosX() + vX / magV * maximumVisibleRange;
            newZ = tower.getPosZ() + vY / magV * maximumVisibleRange;
        }
        return new Position(newX, newZ, heading);
    }

    private double calculateHeading(double oldX, double oldZ, double newX, double newZ) {
        return Math.atan2(newZ - oldZ, newX - oldX);
    }

    private double inMap(double newAxis) {
        if (newAxis >= config.getMapSideSize())
            newAxis = config.getMapSideSize();
        else if (newAxis <= 0)
            newAxis = 0;
        return newAxis;
    }

    public Position calculatePositionAvoidingObject(Locatable offendingObject, Locatable toAvoid, MoveAction action) {
        final var heading = calculateHeading(offendingObject.getPosX(), offendingObject.getPosZ(), action.getPosX(), action.getPosZ());
        final var angleBetweenShapes = calculateHeading(offendingObject.getPosX(), offendingObject.getPosZ(), toAvoid.getPosX(), toAvoid
                .getPosZ());
        final var newX = toAvoid.getPosX() - (toAvoid.getWidth() / 2.0 + offendingObject.getWidth() / 2.0) * Math.cos(angleBetweenShapes);
        final var newZ = toAvoid.getPosZ() - (toAvoid.getHeight() / 2.0 + offendingObject.getHeight() / 2.0) * Math.sin(angleBetweenShapes);
        return new Position(newX, newZ, heading);
    }

    public boolean isColliding(Locatable moved, Locatable toCheck) {
        if (moved.getShapeType() == ShapeType.CIRCLE && toCheck.getShapeType() == ShapeType.CIRCLE) {
            return twoCircleCollision(moved, toCheck);
        } else if (moved.getShapeType() == ShapeType.CIRCLE && toCheck.getShapeType() == ShapeType.RECTANGLE || moved
                .getShapeType() == ShapeType.RECTANGLE && toCheck.getShapeType() == ShapeType.CIRCLE) {
            if (moved.getShapeType() == ShapeType.RECTANGLE)
                return circleAndRectangleCollision(toCheck, moved);
            else
                return circleAndRectangleCollision(moved, toCheck);
        } else if (moved.getShapeType() == ShapeType.RECTANGLE && toCheck.getShapeType() == ShapeType.RECTANGLE) {
            return twoRectangleCollision(moved, toCheck);
        } else
            return false;
    }

    private boolean circleAndRectangleCollision(Locatable circle, Locatable rectangle) {
        var angle = rectangle.getHeading();
        // Rotate circle's center point back
        double unrotatedCircleX = Math.cos(angle) * (circle.getPosX() - rectangle.getPosX()) - Math.sin(angle) * (circle
                .getPosZ() - rectangle.getPosZ()) + rectangle.getPosX();
        double unrotatedCircleY = Math.sin(angle) * (circle.getPosX() - rectangle.getPosX()) + Math.cos(angle) * (circle
                .getPosZ() - rectangle.getPosZ()) + rectangle.getPosZ();
        // Closest point in the rectangle to the center of circle rotated backwards(unrotated)
        double closestX, closestY;
        // Find the unrotated closest x point from center of unrotated circle
        var x = rectangle.getPosX() - rectangle.getWidth() / 2.0;
        if (unrotatedCircleX < x)
            closestX = x;
        else
            closestX = Math.min(unrotatedCircleX, x + rectangle.getWidth());
        // Find the unrotated closest y point from center of unrotated circle
        var y = rectangle.getPosZ() - rectangle.getHeight() / 2.0;
        if (unrotatedCircleY < y)
            closestY = y;
        else
            closestY = Math.min(unrotatedCircleY, y + rectangle.getHeight());
        // Determine collision
        double distance = findDistance(unrotatedCircleX, unrotatedCircleY, closestX, closestY);
        return distance < circle.getWidth() / 2.0; // Collision
    }

    public double findDistance(double fromX, double fromY, double toX, double toY) {
        var a = Math.abs(fromX - toX);
        var b = Math.abs(fromY - toY);
        return Math.sqrt((a * a) + (b * b));
    }

    private boolean twoCircleCollision(Locatable circle1, Locatable circle2) {
        var xDif = circle1.getPosX() - circle2.getPosX();
        var zDif = circle1.getPosZ() - circle2.getPosZ();
        var distanceSquared = xDif * xDif + zDif * zDif;
        return distanceSquared < (radius(circle1) + radius(circle2)) * (radius(circle1) + radius(circle2));
    }

    private double radius(Locatable circle) {
        return circle.getWidth() / 2.0;
    }

    private boolean twoRectangleCollision(Locatable rect1, Locatable rect2) {
        var rectAX1 = rect1.getPosX() - rect1.getWidth() / 2.0;
        var rectBX1 = rect2.getPosX() - rect2.getWidth() / 2.0;
        var rectAX2 = rect1.getPosX() + rect1.getWidth() / 2.0;
        var rectBX2 = rect2.getPosX() + rect2.getWidth() / 2.0;
        var rectAY1 = rect1.getPosZ() - rect1.getHeight() / 2.0;
        var rectBY1 = rect2.getPosZ() - rect2.getHeight() / 2.0;
        var rectAY2 = rect1.getPosZ() + rect1.getHeight() / 2.0;
        var rectBY2 = rect2.getPosZ() + rect2.getHeight() / 2.0;
        return rectAX1 < rectBX2 && rectAX2 > rectBX1 && rectAY1 < rectBY2 && rectAY2 > rectBY1;
    }

    public boolean hasReachedGoal(final Locatable player, final MoveAction moveAction) {
        return Math.abs(player.getPosX() - moveAction.getPosX()) < 1 && Math.abs(player.getPosZ() - moveAction.getPosZ()) < 1;
    }

    public boolean attackBrickShouldContinue(final AttackBrick brick) {
        if (brick.getPosX() <= 0 || brick.getPosX() >= config.getMapSideSize() || brick.getPosZ() <= 0 || brick.getPosZ() >= config
                .getMapSideSize())
            return true;
        else
            return brick.getDistanceToCover() <= 0;
    }

    public boolean isInAttackRangeAndDirection(Gatherer ownGatherer, Locatable toCheck) {
        if (Math.sqrt(Math.pow(toCheck.getPosX() - ownGatherer.getPosX(), 2) + Math
                .pow(toCheck.getPosZ() - ownGatherer.getPosZ(), 2)) <= (ownGatherer.getWidth() * 2)) {
            final var angleBetweenObjects = calculateHeading(ownGatherer.getPosX(), ownGatherer.getPosZ(), toCheck.getPosX(), toCheck
                    .getPosZ());
            return angleBetweenObjects >= ownGatherer.getHeading() - CLOSE_ATTACK_RADIUS_RADIANS && angleBetweenObjects <= ownGatherer
                    .getHeading() + CLOSE_ATTACK_RADIUS_RADIANS;
        }
        return false;
    }

    public boolean isInDefenseRange(Tower ownTower, Locatable toCheck) {
        return Math.sqrt(Math.pow(toCheck.getPosX() - ownTower.getPosX(), 2) + Math
                .pow(toCheck.getPosZ() - ownTower.getPosZ(), 2)) <= (ownTower.getWidth() * 2);
    }

    public double getAttackDistanceToCover(Player player) {
        return player.getTower().getBricks() / config.getRangeAttackDistanceBricksRelation();
    }

    public Position getRandomPositionNearPlayer(Gatherer gatherer) {
        var newX = getNewAxisInMap((Math.random() * (Math.random() % 2 == 0 ? 1 : -1) * 30) + gatherer.getPosX());
        var newZ = getNewAxisInMap((Math.random() * (Math.random() % 2 == 0 ? 1 : -1) * 30) + gatherer.getPosZ());
        return new Position(newX, newZ, 0);
    }

    private double getNewAxisInMap(double newAxis) {
        if (newAxis >= config.getMapSideSize())
            newAxis = config.getMapSideSize();
        else if (newAxis <= 0)
            newAxis = 0;
        return newAxis;
    }
}

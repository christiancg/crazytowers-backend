package com.crazytowers.Utils.auxiliary;

import com.crazytowers.config.ApplicationConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class NumberUtils {
    private final ApplicationConfig config;

    @Autowired
    public NumberUtils(ApplicationConfig config) {
        this.config = config;
    }

    public double getRandomInMap() {
        return Math.random() * config.getMapSideSize();
    }

    public double getRandomAngleInRadians(){
        return Math.toRadians(Math.random() * 360);
    }
}

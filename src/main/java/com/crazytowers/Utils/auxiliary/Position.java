package com.crazytowers.Utils.auxiliary;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Position {
    private double posX;
    private double posZ;
    private double heading;
}
